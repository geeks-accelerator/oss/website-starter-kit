# Website 

Copyright 2019, Geeks Accelerator  
twins@geeksaccelerator.com


## Description

Responsive web application that renders HTML using the `html/template` package from the standard library to enable 
direct interaction with clients and their users. It allows clients to sign up new accounts and provides user 
authentication with HTTP sessions. 

The website service is a fully functioning example. To see screen captures of the Golang web app, check out this Google 
Slides deck:
https://docs.google.com/presentation/d/1WGYqMZ-YUOaNxlZBfU4srpN8i86MU0ppWWSBb3pkejM/edit#slide=id.p

*You are welcome to add comments to the Google Slides.*

We have also deployed this example Go web app to production here:
https://example.saasstartupkit.com

[![Example Golang web app deployed](https://dzuyel7n94hma.cloudfront.net/img/saas-startup-example-golang-project-webapp-projects.png)](https://example.saasstartupkit.com)

The web app relies on the Golang business logic packages developed to provide an API for internal requests. 

Once the website service is running, it will be available on port 3000.

http://127.0.0.1:3000/


## Local Installation

### Build 
```bash
go build .
``` 

### Docker 

To build using the docker file, you need to be in the project root directory since the `Dockerfile` references 
Go Modules that are located there.

```bash
docker build -f cmd/website/Dockerfile -t saas-website .
```


## Getting Started 

### HTTP Pipeline (Middleware)
In any production ready web application there're many concerns that should be handle it correctly such as:
* logging
* tracing
* error handling
* observability metrics
* security

All these responsibilities are orthogonal between each other, and in particular, to the business logic. In `website-starter-kit` these responsabilities are handeled in a chained set of middlewares which allow a clear separation of concerns and it avoids polluting business-rule code.

We can separate existing middlewares in two dimensions: cross-cutting application middlewares, and middlewares for particular routes. Middlewares such as tracing, error handling, and metrics belong to the former category, whereas authentication/authorization to the latter.

If you want to dig into the details regarding these configurations, refer to `handlers/routes.go` where you can find the application middleware chaining, and the particular middlewares per route when adding handlers with `app.Handle(...)`.

### Routes
Every valid URL route can be found in `handlers/route.go`.

Notice that every handler is grouped by business-context (`Projects`, `Users`, `Account`) compared to sharing a single big struct. This allows to limit the scope of action of handlers regarding other actions that are far from its reponsability, and facilitates testing since less mockups will be necessary to test the handlers.

### Future Functionality


