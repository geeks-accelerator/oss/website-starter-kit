module geeks-accelerator/oss/website-starter-kit

require (
	github.com/DataDog/datadog-go v3.4.0+incompatible // indirect
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/aws/aws-sdk-go v1.29.19
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/google/go-cmp v0.4.0
	github.com/gorilla/schema v1.1.0
	github.com/gorilla/sessions v1.2.0
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/ikeikeikeike/go-sitemap-generator/v2 v2.0.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/pborman/uuid v1.2.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.18.0
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.6 // indirect
	github.com/tinylib/msgp v1.1.1 // indirect
	github.com/urfave/cli v1.22.2
	github.com/xwb1989/sqlparser v0.0.0-20180606152119-120387863bf2
	gitlab.com/geeks-accelerator/oss/devops v1.0.65
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.22.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)

// replace gitlab.com/geeks-accelerator/oss/devops => ../devops

go 1.13
