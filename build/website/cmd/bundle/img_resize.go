package bundle

import (
	"bytes"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/nfnt/resize"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

var (
	defaultImgSizes = []int{320, 480, 800, 1024, 1400}
)

type (
	PathFormatter func(s string) string

	imgSrc struct {
		src      string
		srcs     []string
		sizes    []string
		srcset   []string
		setSizes []string
	}
)

func (i imgSrc) Attrs(lazyLoad bool) map[string]string {
	m := make(map[string]string)

	if len(i.setSizes) > 0 {
		if lazyLoad {
			m["data-sizes"] = strings.Join(i.setSizes, ",")
			m["data-srcset"] = strings.Join(i.srcset, ",")
		} else {
			m["sizes"] = strings.Join(i.setSizes, ",")
			m["srcset"] = strings.Join(i.srcset, ",")
		}
	} else {
		if lazyLoad {
			m["data-src"] = i.src
		} else {
			m["src"] = i.src
		}
	}

	return m
}

func (i imgSrc) String(lazyLoad bool) string {
	var attrs []string
	for k, v := range i.Attrs(lazyLoad) {
		attrs = append(attrs, fmt.Sprintf("%s=\"%s\"", k, v))
	}
	return strings.Join(attrs, " ")
}

// ImgSrc returns an srcset for a given image url and defined sizes
// Format the local image path to the fully qualified image URL,
// on stage and prod the app will not have access to the local image
// files if App.StaticS3 is enabled.
func ImgSrc(log zerolog.Logger, pathFormatter PathFormatter, imgDir, imgPath string, sizes []int, includeOrig, lazyLoad, dryRun bool) (imgSrc, error) {
	if pathFormatter == nil {
		pathFormatter = func(s string) string {
			return s
		}
	}

	// Default return value on error.
	resp := imgSrc{
		src: imgPath,
	}

	// Only fully qualified image URLS are supported. On dev the app host should
	// still be included as this lacks the concept of the static directory.
	if strings.HasPrefix(imgPath, "http") {
		return resp, nil
	}

	// If the path is not a full url, update the path to be possibly served from s3/cloudfront.
	resp.src = pathFormatter(imgPath)

	// Determine the file extension for the image path.
	var imgExt string
	if pts := strings.Split(imgPath, "."); len(pts) > 0 {
		imgExt = strings.ToLower(pts[len(pts)-1])
	}

	if imgExt != "jpg" && imgExt != "jpeg" && imgExt != "gif" && imgExt != "png" {
		return resp, nil
	}

	log = log.With().Str("file", imgPath).Str("type", "img").Logger()

	baseImgName := filepath.Base(imgPath)

	expFiles := make(map[int]string)
	for _, s := range sizes {
		pts := strings.Split(baseImgName, ".")
		var uidx int
		if len(pts) >= 2 {
			uidx = len(pts) - 2
		}
		pts[uidx] = fmt.Sprintf("%s-%dw", pts[uidx], s)

		expFiles[s] = strings.Join(pts, ".")
	}

	imgFile := filepath.Join(imgDir, imgPath)
	imgDat, err := ioutil.ReadFile(imgFile)
	if err != nil {
		return resp, errors.WithStack(err)
	}
	size := len(imgDat)

	sort.Ints(sizes)
	maxSize := sizes[len(sizes)-1]

	// Determine the current width of the image, don't need height since will be using
	// maintain the current aspect ratio.
	imgWidth, _, err := getImageDimension(imgDat)
	if includeOrig {
		if imgWidth > maxSize {
			sizes = append(sizes, imgWidth)
		}
	}

	resizeDir := filepath.Dir(imgFile)
	for s, fname := range expFiles {
		if s >= imgWidth {
			continue
		}

		f := func() error {
			// The file path for the newly resized image file.
			thumbPath := filepath.Join(resizeDir, fname)

			// Skip if the file already exists.
			if _, err := os.Stat(thumbPath); err == nil {
				return nil
			}

			var thumbDat []byte
			start := time.Now()

			defer func() {
				savedPct := ((float64(size) - float64(len(thumbDat))) / float64(size)) * 100
				log.Debug().Dur("dur", time.Now().Sub(start)).Float64("saved", savedPct).Msgf("Resized from %d to %d", imgWidth, s)
			}()

			// Render new image with specified width, height of
			// of 0 will preserve the current aspect ratio.
			var err error
			if imgExt == "gif" {
				thumbDat, err = ResizeGif(imgDat, uint(s), 0)
			} else if imgExt == "png" {
				thumbDat, err = ResizePng(imgDat, uint(s), 0)
			} else {
				thumbDat, err = ResizeJpg(imgDat, uint(s), 0)
			}
			if err != nil {
				return errors.WithStack(err)
			}

			if !dryRun {
				err = ioutil.WriteFile(thumbPath, thumbDat, 0644)
				if err != nil {
					return errors.WithStack(err)
				}
			}

			return nil
		}

		err := f()
		if err != nil {
			return resp, err
		}
	}

	for s, fname := range expFiles {
		if s > imgWidth {
			continue
		}

		var nu string
		if s == imgWidth {
			nu = pathFormatter(imgPath)
		} else {
			nk := filepath.Join(filepath.Dir(imgPath), fname)
			nu = pathFormatter(nk)
		}

		resp.srcset = append(resp.srcset, fmt.Sprintf("%s %dw", nu, s))
		resp.srcs = append(resp.srcs, nu)
		resp.sizes = append(resp.sizes, strconv.Itoa(s))
		if s == maxSize {
			resp.setSizes = append(resp.setSizes, fmt.Sprintf("%dpx", s))
			resp.src = nu
		} else {
			resp.setSizes = append(resp.setSizes, fmt.Sprintf("(max-width: %dpx) %dpx", s, s))
		}
	}

	return resp, nil
}

// ResizeJpg resizes a JPG image file to specified width and height using
// lanczos resampling and preserving the aspect ratio.
func ResizeJpg(dat []byte, width, height uint) ([]byte, error) {
	// decode jpeg into image.Image
	img, err := jpeg.Decode(bytes.NewReader(dat))
	if err != nil {
		return []byte{}, errors.WithStack(err)
	}

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(width, height, img, resize.NearestNeighbor)

	// write new image to file
	var out = new(bytes.Buffer)
	jpeg.Encode(out, m, nil)

	return out.Bytes(), nil
}

// ResizeGif resizes a GIF image file to specified width and height using
// lanczos resampling and preserving the aspect ratio.
func ResizeGif(dat []byte, width, height uint) ([]byte, error) {
	// decode gif into image.Image
	img, err := gif.Decode(bytes.NewReader(dat))
	if err != nil {
		return []byte{}, errors.WithStack(err)
	}

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(width, height, img, resize.NearestNeighbor)

	// write new image to file
	var out = new(bytes.Buffer)
	gif.Encode(out, m, nil)

	return out.Bytes(), nil
}

// ResizePng resizes a PNG image file to specified width and height using
// lanczos resampling and preserving the aspect ratio.
func ResizePng(dat []byte, width, height uint) ([]byte, error) {
	// decode png into image.Image
	img, err := png.Decode(bytes.NewReader(dat))
	if err != nil {
		return []byte{}, errors.WithStack(err)
	}

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(width, height, img, resize.NearestNeighbor)

	// write new image to file
	var out = new(bytes.Buffer)
	png.Encode(out, m)

	return out.Bytes(), nil
}

// getImageDimension returns the width and height for a given local file path
func getImageDimension(dat []byte) (int, int, error) {
	image, _, err := image.DecodeConfig(bytes.NewReader(dat))
	if err != nil {
		return 0, 0, errors.WithStack(err)
	}
	return image.Width, image.Height, nil
}
