package main

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"expvar"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"reflect"
	"strings"
	"syscall"
	"time"

	"geeks-accelerator/oss/website-starter-kit/cmd/website/handlers"
	"geeks-accelerator/oss/website-starter-kit/internal/mid"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/flag"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/tmplrender"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/webcontext"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/weberror"
	"geeks-accelerator/oss/website-starter-kit/internal/webroute"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/geeks-accelerator/oss/devops/pkg/devdeploy"
	"golang.org/x/crypto/acme"
	"golang.org/x/crypto/acme/autocert"
)

// build is the git version of this program. It is set using build flags in the makefile.
var build = "develop"

// service is the name of the program used for logging, tracing and the
// the prefix used for loading env variables
// ie: export WEBSITE_ENV=dev
var service = "WEBSITE"

func main() {

	// =========================================================================
	// Logging
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)
	log.SetPrefix(service + " : ")
	log := log.New(os.Stdout, log.Prefix(), log.Flags())

	// =========================================================================
	// Configuration
	var cfg struct {
		Env  string `default:"dev" envconfig:"ENV"`
		HTTP struct {
			Host         string        `default:"0.0.0.0:3000" envconfig:"HOST"`
			ReadTimeout  time.Duration `default:"10s" envconfig:"READ_TIMEOUT"`
			WriteTimeout time.Duration `default:"10s" envconfig:"WRITE_TIMEOUT"`
		}
		HTTPS struct {
			Host         string        `default:"" envconfig:"HOST"`
			ReadTimeout  time.Duration `default:"5s" envconfig:"READ_TIMEOUT"`
			WriteTimeout time.Duration `default:"5s" envconfig:"WRITE_TIMEOUT"`
		}
		Service struct {
			Name        string   `default:"website" envconfig:"SERVICE_NAME"`
			BaseUrl     string   `default:"" envconfig:"BASE_URL"  example:"http://example.saasstartupkit.com"`
			HostNames   []string `envconfig:"HOST_NAMES" example:"www.example.saasstartupkit.com"`
			EnableHTTPS bool     `default:"false" envconfig:"ENABLE_HTTPS"`
			TemplateDir string   `default:"./templates" envconfig:"TEMPLATE_DIR"`
			StaticFiles struct {
				Dir               string `default:"./static" envconfig:"STATIC_DIR"`
				S3Enabled         bool   `envconfig:"S3_ENABLED"`
				S3Prefix          string `default:"public/web_app/static" envconfig:"S3_PREFIX"`
				CloudFrontEnabled bool   `envconfig:"CLOUDFRONT_ENABLED"`
				ImgResizeEnabled  bool   `envconfig:"IMG_RESIZE_ENABLED"`
			}
			Minify          bool          `envconfig:"MINIFY"`
			DebugHost       string        `default:"0.0.0.0:4000" envconfig:"DEBUG_HOST"`
			ShutdownTimeout time.Duration `default:"5s" envconfig:"SHUTDOWN_TIMEOUT"`
			ScaleToZero     time.Duration `envconfig:"SCALE_TO_ZERO"`
		}
		Project struct {
			Name string `default:"" envconfig:"PROJECT_NAME"`
		}
		Aws struct {
			AccessKeyID                string `envconfig:"AWS_ACCESS_KEY_ID"`              // WEB_API_AWS_AWS_ACCESS_KEY_ID or AWS_ACCESS_KEY_ID
			SecretAccessKey            string `envconfig:"AWS_SECRET_ACCESS_KEY" json:"-"` // don't print
			Region                     string `default:"us-west-2" envconfig:"AWS_DEFAULT_REGION"`
			S3BucketPrivate            string `envconfig:"S3_BUCKET_PRIVATE"`
			S3BucketPublic             string `envconfig:"S3_BUCKET_PUBLIC"`
			SecretsManagerConfigPrefix string `default:"" envconfig:"SECRETS_MANAGER_CONFIG_PREFIX"`

			// Get an AWS session from an implicit source if no explicit
			// configuration is provided. This is useful for taking advantage of
			// EC2/ECS instance roles.
			UseRole bool `envconfig:"AWS_USE_ROLE"`
		}
		BuildInfo struct {
			CiCommitRefName  string `envconfig:"CI_COMMIT_REF_NAME"`
			CiCommitShortSha string `envconfig:"CI_COMMIT_SHORT_SHA"`
			CiCommitSha      string `envconfig:"CI_COMMIT_SHA"`
			CiCommitTag      string `envconfig:"CI_COMMIT_TAG"`
			CiJobId          string `envconfig:"CI_JOB_ID"`
			CiJobUrl         string `envconfig:"CI_JOB_URL"`
			CiPipelineId     string `envconfig:"CI_PIPELINE_ID"`
			CiPipelineUrl    string `envconfig:"CI_PIPELINE_URL"`
		}
	}

	// For additional details refer to https://github.com/kelseyhightower/envconfig
	if err := envconfig.Process(service, &cfg); err != nil {
		log.Fatalf("main : Parsing Config : %+v", err)
	}

	if err := flag.Process(&cfg); err != nil {
		if err != flag.ErrHelp {
			log.Fatalf("main : Parsing Command Line : %+v", err)
		}
		return // We displayed help.
	}

	// =========================================================================
	// Config Validation & Defaults

	// AWS access keys are required, if roles are enabled, remove any placeholders.
	if cfg.Aws.UseRole {
		cfg.Aws.AccessKeyID = ""
		cfg.Aws.SecretAccessKey = ""

		// Get an AWS session from an implicit source if no explicit
		// configuration is provided. This is useful for taking advantage of
		// EC2/ECS instance roles.
		if cfg.Aws.Region == "" {
			sess := session.Must(session.NewSession())
			md := ec2metadata.New(sess)

			var err error
			cfg.Aws.Region, err = md.Region()
			if err != nil {
				log.Fatalf("main : Load region of ecs metadata : %+v", err)
			}
		}
	}

	// Set the default AWS Secrets Manager prefix used for name to store config files that will be persisted across
	// deployments and distributed to each instance of the service running.
	if cfg.Aws.SecretsManagerConfigPrefix == "" {
		var pts []string
		if cfg.Project.Name != "" {
			pts = append(pts, cfg.Project.Name)
		}
		pts = append(pts, cfg.Env)

		cfg.Aws.SecretsManagerConfigPrefix = filepath.Join(pts...)
	}

	// If base URL is empty, set the default value from the HTTP Host
	if cfg.Service.BaseUrl == "" {
		baseUrl := cfg.HTTP.Host
		if !strings.HasPrefix(baseUrl, "http") {
			if strings.HasPrefix(baseUrl, "0.0.0.0:") {
				pts := strings.Split(baseUrl, ":")
				pts[0] = "127.0.0.1"
				baseUrl = strings.Join(pts, ":")
			} else if strings.HasPrefix(baseUrl, ":") {
				baseUrl = "127.0.0.1" + baseUrl
			}
			baseUrl = "http://" + baseUrl
		}
		cfg.Service.BaseUrl = baseUrl
	}

	// When HTTPS is not specifically enabled, but an HTTP host is set, enable HTTPS.
	if !cfg.Service.EnableHTTPS && cfg.HTTPS.Host != "" {
		cfg.Service.EnableHTTPS = true
	}

	// Determine the primary host by parsing host from the base app URL.
	baseSiteUrl, err := url.Parse(cfg.Service.BaseUrl)
	if err != nil {
		log.Fatalf("main : Parse service base URL : %s : %+v", cfg.Service.BaseUrl, err)
	}

	// Drop any ports from the base app URL.
	var primaryServiceHost string
	if strings.Contains(baseSiteUrl.Host, ":") {
		primaryServiceHost, _, err = net.SplitHostPort(baseSiteUrl.Host)
		if err != nil {
			log.Fatalf("main : SplitHostPort : %s : %+v", baseSiteUrl.Host, err)
		}
	} else {
		primaryServiceHost = baseSiteUrl.Host
	}

	// =========================================================================
	// Log Service Info

	// Print the build version for our logs. Also expose it under /debug/vars.
	expvar.NewString("build").Set(build)
	log.Printf("main : Started : Service Initializing version %q", build)
	defer log.Println("main : Completed")

	// Print the config for our logs. It's important to any credentials in the config
	// that could expose a security risk are excluded from being json encoded by
	// applying the tag `json:"-"` to the struct var.
	{
		cfgJSON, err := json.MarshalIndent(cfg, "", "    ")
		if err != nil {
			log.Fatalf("main : Marshalling Config to JSON : %+v", err)
		}
		log.Printf("main : Config : %v\n", string(cfgJSON))
	}

	// =========================================================================
	// Init AWS Session
	var awsSession *session.Session
	if cfg.Aws.UseRole {
		// Get an AWS session from an implicit source if no explicit
		// configuration is provided. This is useful for taking advantage of
		// EC2/ECS instance roles.
		awsSession = session.Must(session.NewSession())
		if cfg.Aws.Region != "" {
			awsSession.Config.WithRegion(cfg.Aws.Region)
		}

		log.Printf("main : AWS : Using role.\n")

	} else if cfg.Aws.AccessKeyID != "" {
		creds := credentials.NewStaticCredentials(cfg.Aws.AccessKeyID, cfg.Aws.SecretAccessKey, "")
		awsSession = session.New(&aws.Config{Region: aws.String(cfg.Aws.Region), Credentials: creds})

		log.Printf("main : AWS : Using static credentials\n")
	}

	// =========================================================================
	// Init repositories and AppContext

	webRoute, err := webroute.New(cfg.Service.BaseUrl)
	if err != nil {
		log.Fatalf("main : checklist routes : %+v", cfg.Service.BaseUrl, err)
	}

	appCtx := &handlers.AppContext{
		Log:         log,
		Env:         cfg.Env,
		TemplateDir: cfg.Service.TemplateDir,
		StaticDir:   cfg.Service.StaticFiles.Dir,
		WebRoute:    webRoute,
	}

	// =========================================================================
	// Load middlewares that need to be configured specific for the service.

	// Init redirect middleware to ensure all requests go to the primary domain contained in the base URL.
	if primaryServiceHost != "127.0.0.1" && primaryServiceHost != "localhost" {
		redirect := mid.DomainNameRedirect(mid.DomainNameRedirectConfig{
			RedirectConfig: mid.RedirectConfig{
				Code: http.StatusMovedPermanently,
				Skipper: func(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) bool {
					if r.URL.Path == "/ping" {
						return true
					}
					return false
				},
			},
			DomainName:   primaryServiceHost,
			HTTPSEnabled: cfg.Service.EnableHTTPS,
		})
		appCtx.PostAppMiddleware = append(appCtx.PostAppMiddleware, redirect)
	}

	// Apply response minification if enabled.
	if cfg.Service.Minify {
		appCtx.PostAppMiddleware = append(appCtx.PostAppMiddleware, mid.Minify())
	}

	// =========================================================================
	// URL Formatter

	// s3UrlFormatter is a help function used by to convert an s3 key to
	// a publicly available image URL.
	var staticS3UrlFormatter func(string) string
	if cfg.Service.StaticFiles.S3Enabled || cfg.Service.StaticFiles.CloudFrontEnabled || cfg.Service.StaticFiles.ImgResizeEnabled {
		s3UrlFormatter, err := devdeploy.S3UrlFormatter(awsSession, cfg.Aws.S3BucketPublic, cfg.Service.StaticFiles.S3Prefix, cfg.Service.StaticFiles.CloudFrontEnabled)
		if err != nil {
			log.Fatalf("main : S3UrlFormatter failed : %+v", err)
		}

		staticS3UrlFormatter = func(p string) string {
			// When the path starts with a forward slash its referencing a local file,
			// make sure the static file prefix is included
			if (strings.HasPrefix(p, "/") || !strings.HasPrefix(p, "://")) && !strings.HasPrefix(p, cfg.Service.StaticFiles.S3Prefix) {
				p = filepath.Join(cfg.Service.StaticFiles.S3Prefix, p)
			}

			return s3UrlFormatter(p)
		}
	} else {
		staticS3UrlFormatter = webRoute.WebsiteUrl
	}

	// staticUrlFormatter is a help function used by template functions defined below.
	// If the app has an S3 bucket defined for the static directory, all references in the app
	// templates should be updated to use a fully qualified URL for either the public file on S3
	// on from the cloudfront distribution.
	var staticUrlFormatter func(string) string
	if cfg.Service.StaticFiles.S3Enabled || cfg.Service.StaticFiles.CloudFrontEnabled {
		staticUrlFormatter = staticS3UrlFormatter
	}

	// =========================================================================
	// Template Renderer
	// Implements interface web.Renderer to support alternative renderer

	// Append query string value to break browser cache used for services
	// that render responses for a browser with the following:
	// 	1. when env=dev, the current timestamp will be used to ensure every
	// 		request will skip browser cache.
	// 	2. all other envs, ie stage and prod. The commit hash will be used to
	// 		ensure that all cache will be reset with each new deployment.
	browserCacheBusterQueryString := func() string {
		var v string
		if cfg.Env == "dev" {
			// On dev always break cache.
			v = fmt.Sprintf("%d", time.Now().UTC().Unix())
		} else {
			// All other envs, use the current commit hash for the build
			v = cfg.BuildInfo.CiCommitSha
		}
		return v
	}

	// Helper method for appending the browser cache buster as a query string to
	// support breaking browser cache when necessary
	browserCacheBusterFunc := browserCacheBuster(browserCacheBusterQueryString)

	// Need defined functions below since they require config values, able to add additional functions
	// here to extend functionality.
	tmplFuncs := template.FuncMap{
		// BuildInfo returns the specific key from BuildInfo set in the current config.
		"BuildInfo": func(k string) string {
			r := reflect.ValueOf(cfg.BuildInfo)
			f := reflect.Indirect(r).FieldByName(k)
			return f.String()
		},
		// SiteBaseUrl returns the absolute URL for a relative path using the base site URL set in the config.
		"SiteBaseUrl": func(p string) string {
			u, err := url.Parse(cfg.Service.BaseUrl)
			if err != nil {
				return "?"
			}
			u.Path = p
			return u.String()
		},
		// SiteAssetUrl returns the absolute URL for a relative path that either served locally, from the public S3
		// bucket or from Cloudfront depending on the current config settings for StaticFiles. The defined static asset
		// prefix to the path defined in the current config settings for StaticFiles. The response URL includes a cache
		// busting query param based on the current commit.
		"SiteAssetUrl": func(p string) string {
			var u string
			if staticUrlFormatter != nil {
				u = staticUrlFormatter(p)
			} else {
				if !strings.HasPrefix(p, "/") {
					p = "/" + p
				}
				u = p
			}

			u = browserCacheBusterFunc(u)

			return u
		},
		// SiteS3Url returns the URL that for an S3 Key path that has been uploaded to S3 to the specific public s3 key
		// prefix that returns the absolute S3 URL or the CloudFront equivalent if enabled.
		"SiteS3Url": func(p string) string {
			var u string
			if staticUrlFormatter != nil {
				u = staticUrlFormatter(filepath.Join(cfg.Service.Name, p))
			} else {
				u = p
			}
			return u
		},
		// S3Url returns the URL that for an S3 Key path that has been uploaded to S3 to the public s3 key
		// prefix that returns the absolute S3 URL or the CloudFront equivalent if enabled.
		"S3Url": func(p string) string {
			var u string
			if staticUrlFormatter != nil {
				u = staticUrlFormatter(p)
			} else {
				u = p
			}
			return u
		},
		// ErrorMessage returns the error message that is formatted as a response for end users to consume.
		"ErrorMessage": func(ctx context.Context, err error) string {
			werr, ok := err.(*weberror.Error)
			if ok {
				if werr.Message != "" {
					return werr.Message
				}
				return werr.Error()
			}
			return fmt.Sprintf("%s", err)
		},
		// ErrorDetails returns the full error for dev and stage envs to help with debugging.
		"ErrorDetails": func(ctx context.Context, err error) string {
			var displayFullError bool
			switch webcontext.ContextEnv(ctx) {
			case webcontext.Env_Dev, webcontext.Env_Stage:
				displayFullError = true
			}

			if !displayFullError {
				return ""
			}

			werr, ok := err.(*weberror.Error)
			if ok {
				if werr.Cause != nil {
					return fmt.Sprintf("%s\n%+v", werr.Error(), werr.Cause)
				}

				return fmt.Sprintf("%+v", werr.Error())
			}

			return fmt.Sprintf("%+v", err)
		},
	}

	imgUrlFormatter := staticUrlFormatter
	if imgUrlFormatter == nil {
		baseUrl, err := url.Parse(cfg.Service.BaseUrl)
		if err != nil {
			log.Fatalf("main : url Parse(%s) : %+v", cfg.Service.BaseUrl, err)
		}

		imgUrlFormatter = func(p string) string {
			if strings.HasPrefix(p, "http") {
				return p
			}
			baseUrl.Path = p
			return baseUrl.String()
		}
	}

	tmplFuncs["S3ImgUrl"] = func(ctx context.Context, p string, size int) string {
		return imgUrlFormatter(p)
	}

	//
	t := tmplrender.NewTemplate(tmplFuncs)

	// global variables exposed for rendering of responses with templates
	gvd := map[string]interface{}{
		"_Service": map[string]interface{}{
			"ENV":          cfg.Env,
			"BuildInfo":    cfg.BuildInfo,
			"BuildVersion": build,
		},
	}

	// Custom error handler to support rendering user friendly error page for improved web experience.
	eh := func(ctx context.Context, w http.ResponseWriter, r *http.Request, renderer web.Renderer, statusCode int, er error) error {
		if statusCode == 0 {
			if webErr, ok := er.(*weberror.Error); ok {
				statusCode = webErr.Status
			}
		}

		if web.RequestIsImage(r) {
			return err
		}

		return web.RenderError(ctx, w, r, er, renderer, handlers.TmplLayoutBase, handlers.TmplContentErrorGeneric, web.MIMETextHTMLCharsetUTF8)
	}

	// Enable template renderer to reload and parse template files when generating a response of dev
	// for a more developer friendly process. Any changes to the template files will be included
	// without requiring re-build/re-start of service.
	// This only supports files that already exist, if a new template file is added, then the
	// serivce needs to be restarted, but not rebuilt.
	enableHotReload := cfg.Env == "dev"
	enableOptimizations := true
	metaNoFollow := cfg.Env != webcontext.Env_Prod

	// Template Renderer used to generate HTML response for web experience.
	appCtx.Renderer, err = tmplrender.NewTemplateRenderer(cfg.Service.TemplateDir, enableHotReload, enableOptimizations, metaNoFollow, gvd, t, eh)
	if err != nil {
		log.Fatalf("main : Marshalling Config to JSON : %+v", err)
	}

	// =========================================================================
	// Start Debug Service. Not concerned with shutting this down when the
	// application is being shutdown.
	//
	// /debug/vars - Added to the default mux by the expvars package.
	// /debug/pprof - Added to the default mux by the net/http/pprof package.
	if cfg.Service.DebugHost != "" {
		go func() {
			log.Printf("main : Debug Listening %s", cfg.Service.DebugHost)
			log.Printf("main : Debug Listener closed : %v", http.ListenAndServe(cfg.Service.DebugHost, http.DefaultServeMux))
		}()
	}

	// =========================================================================
	// ECS Task registration for services that don't use an AWS Elastic Load Balancer.
	err = devdeploy.EcsServiceTaskInit(log, awsSession)
	if err != nil {
		log.Fatalf("main : Ecs Service Task init : %+v", err)
	}

	// Optional to scale down the service after X seconds. This is useful for a stage env when after 1 hours, review
	// should have been completed. The service will automatically scale back to zero for avoid extra costs. The
	// deploy stage can always be executed again to bring the service back online.
	if cfg.Service.ScaleToZero.Seconds() > 0 {
		log.Printf("Scaling service to 0 tasks after %v seconds\n", cfg.Service.ScaleToZero.Seconds())

		exitTimer := time.NewTimer(cfg.Service.ScaleToZero)
		go func() {
			<-exitTimer.C
			log.Printf("exitTimer expired, scaling awsSession task to 0")
			err = devdeploy.EcsServiceSetDesiredCount(log, awsSession, 0)
			if err != nil {
				log.Fatalf("main : Ecs Service scale down : %+v", err)
			}
		}()
	}

	// =========================================================================
	// Start APP Service

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Make an list of HTTP servers for both HTTP and HTTPS requests.
	var httpServers []http.Server

	// Start the HTTP service listening for requests.
	if cfg.HTTP.Host != "" {
		api := http.Server{
			Addr:           cfg.HTTP.Host,
			Handler:        handlers.APP(shutdown, appCtx),
			ReadTimeout:    cfg.HTTP.ReadTimeout,
			WriteTimeout:   cfg.HTTP.WriteTimeout,
			MaxHeaderBytes: 1 << 20,
		}
		httpServers = append(httpServers, api)

		go func() {
			log.Printf("main : APP Listening %s", cfg.HTTP.Host)
			serverErrors <- api.ListenAndServe()
		}()
	}

	// Start the HTTPS service listening for requests with an SSL Cert auto generated with Let's Encrypt.
	if cfg.HTTPS.Host != "" {
		api := http.Server{
			Addr:           cfg.HTTPS.Host,
			Handler:        handlers.APP(shutdown, appCtx),
			ReadTimeout:    cfg.HTTPS.ReadTimeout,
			WriteTimeout:   cfg.HTTPS.WriteTimeout,
			MaxHeaderBytes: 1 << 20,
		}

		// Generate a unique list of hostnames.
		var hosts []string
		if primaryServiceHost != "" {
			hosts = append(hosts, primaryServiceHost)
		}
		for _, h := range cfg.Service.HostNames {
			h = strings.TrimSpace(h)
			if h != "" && h != primaryServiceHost {
				hosts = append(hosts, h)
			}
		}

		// Enable autocert to store certs via Secret Manager.
		secretPrefix := filepath.Join(cfg.Aws.SecretsManagerConfigPrefix, "autocert")

		// Local file cache to reduce requests hitting Secret Manager.
		localCache := autocert.DirCache(os.TempDir())

		cache, err := devdeploy.NewSecretManagerAutocertCache(log, awsSession, secretPrefix, localCache)
		if err != nil {
			log.Fatalf("main : HTTPS : %+v", err)
		}

		m := &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(hosts...),
			Cache:      cache,
		}
		api.TLSConfig = &tls.Config{GetCertificate: m.GetCertificate}
		api.TLSConfig.NextProtos = append(api.TLSConfig.NextProtos, acme.ALPNProto)

		httpServers = append(httpServers, api)

		go func() {
			log.Printf("main : APP Listening %s with SSL cert for hosts %s", cfg.HTTPS.Host, strings.Join(hosts, ", "))
			serverErrors <- api.ListenAndServeTLS("", "")
		}()
	}

	// =========================================================================
	// Shutdown

	// Blocking main and waiting for shutdown.
	select {
	case err := <-serverErrors:
		log.Fatalf("main : Error starting server: %+v", err)

	case sig := <-shutdown:
		log.Printf("main : %v : Start shutdown..", sig)

		// Create context for Shutdown call.
		ctx, cancel := context.WithTimeout(context.Background(), cfg.Service.ShutdownTimeout)
		defer cancel()

		// Handle closing connections for both possible HTTP servers.
		for _, api := range httpServers {

			// Asking listener to shutdown and load shed.
			err := api.Shutdown(ctx)
			if err != nil {
				log.Printf("main : Graceful shutdown did not complete in %v : %v", cfg.Service.ShutdownTimeout, err)
				err = api.Close()
			}

		}

		// Log the status of this shutdown.
		switch {
		case sig == syscall.SIGSTOP:
			log.Fatal("main : Integrity issue caused shutdown")
		case err != nil:
			log.Fatalf("main : Could not stop server gracefully : %+v", err)
		}
	}
}

// browserCacheBuster appends a the query string param v to a given url with
// a value based on the value returned from cacheBusterValueFunc
func browserCacheBuster(cacheBusterValueFunc func() string) func(uri string) string {
	f := func(uri string) string {
		v := cacheBusterValueFunc()
		if v == "" {
			return uri
		}

		u, err := url.Parse(uri)
		if err != nil {
			return ""
		}
		q := u.Query()
		q.Set("v", v)
		u.RawQuery = q.Encode()

		return u.String()
	}

	return f
}
